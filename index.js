const colorizeElement = (node) => {
  console.log(node);
  node.classList.add("new-class");
  node.setAttribute("data-tag", node.tagName);
  node.style.color = "#f1f1f1";
  node.style.fontSize = "12px";
  return node;
};

module.exports = colorizeElement;
